INSERT INTO user (id, password, user_role, username) VALUES
(1, '$2a$10$s897QkZrXIeV0DbvzYJtsOeQzLbecDfP3XuvthTMDyhGTeBXX6p4O', 'USER', 'user'),
(2, '$2a$10$okHTTFBrr.wdDjdlAXAAkODSW8bpdaZALdsCP7rM7PwErzABmpsO.', 'ADMIN', 'admin');
INSERT INTO army (id, user_id) VALUES
(1,1),
(2,2);
INSERT INTO treasury (id, food, stone, wood, user_id) VALUES
(1, 50, 50, 50,1),
(2, 50, 50, 50, 2);
INSERT INTO villages (vilage_id, user_id) VALUES
(1,1);