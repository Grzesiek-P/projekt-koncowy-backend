package pl.cm.finalprojectbackend.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.army.repository.UnitRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;

import javax.transaction.Transactional;


@Service
public class CostManager {

    private final UnitRepository unitRepository;
    private final ArmyRepository armyRepository;
    private final TreasuryService treasuryService;
    private final GameConfigurationService gameConfigurationService;


    @Autowired
    public CostManager(UnitRepository unitRepository,
                       ArmyRepository armyRepository,
                       TreasuryService treasuryService,
                       GameConfigurationService gameConfigurationService) {
        this.unitRepository = unitRepository;
        this.armyRepository = armyRepository;
        this.treasuryService = treasuryService;
        this.gameConfigurationService = gameConfigurationService;
    }

    @Transactional
    public void manageExpenses() {

        armyRepository.findAll()
                .forEach(army -> {
                    army.getUnits()
                            .forEach(unit -> {
                                if(!treasuryService.payCost(army.getUser(), gameConfigurationService.getUnitBoardCost(unit.getUnitType()))) {
                                    unitRepository.delete(unit);
                                }
                            });
                });
    }


}
