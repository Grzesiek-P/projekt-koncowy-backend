package pl.cm.finalprojectbackend.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.Building;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.building.repository.BuildingRepository;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProfitManager {

    private final UserRepository userRepository;
    private final VillageRepository villageRepository;
    private final BuildingRepository buildingRepository;
    private final TreasuryService treasuryService;
    private  final GameConfigurationService gameConfigurationService;


    @Autowired
    public ProfitManager(UserRepository userRepository,
                              VillageRepository villageRepository,
                              BuildingRepository buildingRepository,
                              TreasuryService treasuryService,
                              GameConfigurationService gameConfigurationService) {
        this.userRepository = userRepository;
        this.villageRepository = villageRepository;
        this.buildingRepository = buildingRepository;
        this.treasuryService = treasuryService;
        this.gameConfigurationService = gameConfigurationService;
    }

    @Transactional
    public void manageIncomes() {

        List<Village> villages = villageRepository.findAll();
        villages
                .forEach(village -> {
                    List<Building> buildings = village.getBuildings();
                    buildings
                            .forEach(building -> {
                                treasuryService.payCost(village.getUser(), gameConfigurationService.getProduction(building.getBuildingType()));
                            });
                });
    }


}
