package pl.cm.finalprojectbackend.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class ScheduledFixedRate {

    private final ProfitManager profitManager;
    private final CostManager costManager;


    @Autowired
    public ScheduledFixedRate(ProfitManager profitManager, CostManager costManager) {
        this.profitManager = profitManager;
        this.costManager = costManager;
    }

    @Scheduled(fixedRate = 10000)
    public void scheduleFixedRateTask() {
        profitManager.manageIncomes();
        costManager.manageExpenses();
    }


}
