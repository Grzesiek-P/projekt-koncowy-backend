package pl.cm.finalprojectbackend.auth.exeption;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NotEnoughCredentials extends RuntimeException {

}
