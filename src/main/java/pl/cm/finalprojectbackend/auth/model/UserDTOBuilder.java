package pl.cm.finalprojectbackend.auth.model;

public class UserDTOBuilder {
    private Integer id;
    private String username;
    private String password;
    private UserRole userRole;

    public UserDTOBuilder withId(Integer id){
        this.id = id;
        return this;
    }

    public UserDTOBuilder withUsername(String username){
        this.username = username;
        return this;
    }

    public UserDTOBuilder withPassword(String password){
        this.password = password;
        return this;
    }

    public UserDTOBuilder withUserRole(UserRole userRole){
        this.userRole = userRole;
        return this;
    }

    public UserDTO build(){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(this.id);
        userDTO.setPassword(this.password);
        userDTO.setUsername(this.username);
        userDTO.setUserRole(this.userRole);

        return userDTO;
    }

    public UserDTO convertUserToUserDTO(User user){
        return withId(user.getId()).withUsername(user.getUsername()).withPassword(user.getPassword()).withUserRole(user.getUserRole()).build();
    }
}
