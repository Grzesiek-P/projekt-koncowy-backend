package pl.cm.finalprojectbackend.auth.model;

public enum UserRole {
    ADMIN,
    USER
}
