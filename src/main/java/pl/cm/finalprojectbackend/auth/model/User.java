package pl.cm.finalprojectbackend.auth.model;

import lombok.Getter;
import lombok.Setter;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.resources.model.Treasury;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    private String username;

    private String password;

    @Enumerated(value = EnumType.STRING)
    private UserRole userRole;

    @OneToOne (mappedBy = "user", cascade = CascadeType.REMOVE)
    @Getter
    @Setter
    private Treasury treasury;

    @OneToOne (mappedBy = "user", cascade = CascadeType.REMOVE)
    @Getter
    @Setter
    private Army army;

    @OneToOne (mappedBy = "user", cascade = CascadeType.REMOVE)
    @Getter
    @Setter
    private Village village;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
