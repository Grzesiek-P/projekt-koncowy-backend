package pl.cm.finalprojectbackend.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.auth.exeption.UserAlreadyExists;
import pl.cm.finalprojectbackend.auth.exeption.UserNotFoundException;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.model.UserRole;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.model.Treasury;
import pl.cm.finalprojectbackend.resources.repository.TreasuryRepository;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final ArmyRepository armyRepository;
    private final VillageRepository villageRepository;
    private final TreasuryRepository treasuryRepository;
    private final GameConfigurationService gameConfigurationService;

    @Autowired
    public UserService(GameConfigurationService gameConfigurationService, UserRepository userRepository, VillageRepository villageRepository, PasswordEncoder passwordEncoder, ArmyRepository armyRepository, TreasuryRepository treasuryRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.armyRepository = armyRepository;
        this.villageRepository = villageRepository;
        this.treasuryRepository = treasuryRepository;
        this.gameConfigurationService = gameConfigurationService;
    }


    public List<User> getUsers() {

        return userRepository.findAll();

    }


    public User addUser(String userName, String userPassword) {
        if (userRepository.findByUsername(userName) != null) {
            throw new UserAlreadyExists();
        }
        User user = new User();
        user.setUsername(userName);
        user.setPassword(passwordEncoder.encode(userPassword));
        user.setUserRole(UserRole.USER);

        userRepository.save(user);

        Treasury treasury = gameConfigurationService.getInitialTreasury();
        treasury.setUser(user);
        treasuryRepository.save(treasury);

        Village village = new Village();
        village.setUser(user);
        villageRepository.save(village);

        Army army = new Army();
        List<Unit> units = new ArrayList<>();
        army.setUnits(units);
        army.setUser(user);
        armyRepository.save(army);

        return user;
    }

    public User deleteUser(Integer id, Principal principal) {

        User loggedUser = userRepository.findByUsername(principal.getName());
        Optional<User> optionalUser = userRepository.findById(id);

        if (optionalUser.isEmpty()) {
            throw new UserNotFoundException("User not found");
        }

        User user = optionalUser.get();

        if ( (loggedUser.getUserRole().equals(UserRole.USER) && loggedUser.equals(user)) ||
                    loggedUser.getUserRole().equals(UserRole.ADMIN))
             {
                 userRepository.delete(user);

                 return loggedUser;
             }

        throw new NotEnoughCredentials();

    }

    public boolean authorisedUser(Principal principal) {
        return (principal != null);
    }
}

