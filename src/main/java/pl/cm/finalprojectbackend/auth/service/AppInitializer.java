package pl.cm.finalprojectbackend.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.model.UserRole;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.resources.model.Treasury;
import pl.cm.finalprojectbackend.resources.repository.TreasuryRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AppInitializer {
    private final UserRepository userRepository;
    private final ArmyRepository armyRepository;
    private final VillageRepository villageRepository;
    private final TreasuryRepository treasuryRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AppInitializer(PasswordEncoder passwordEncoder, UserRepository userRepository, ArmyRepository armyRepository, TreasuryRepository treasuryRepository, VillageRepository villageRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.armyRepository = armyRepository;
        this.villageRepository = villageRepository;
        this.treasuryRepository = treasuryRepository;
    }

    @Bean
    ApplicationRunner applicationRunner() {
        return args -> {
            if(userRepository.findByUsername("user")==null){
                User user = new User();
                user.setId(1);
                user.setUsername("user");
                user.setPassword(passwordEncoder.encode("password"));
                user.setUserRole(UserRole.USER);
                userRepository.save(user);

                Treasury treasury = new Treasury();
                treasury.setUser(user);
                treasuryRepository.save(treasury);

                Village village = new Village();
                village.setUser(user);
                villageRepository.save(village);

                User user1 = new User();
                user1.setId(2);
                user1.setUsername("admin");
                user1.setPassword(passwordEncoder.encode("password"));
                user1.setUserRole(UserRole.ADMIN);
                userRepository.save(user1);

                Treasury treasury1 = new Treasury();
                treasury1.setUser(user1);
                treasuryRepository.save(treasury1);

                Army army = new Army();
                List<Unit> units = new ArrayList<>();
                army.setUnits(units);
                army.setId(1);
                army.setUser(user);
                armyRepository.save(army);

                Army army1 = new Army();
                List<Unit> units1 = new ArrayList<>();
                army1.setUnits(units1);
                army1.setId(2);
                army1.setUser(user1);
                armyRepository.save(army1);
            }

        };
    }
}
