package pl.cm.finalprojectbackend.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.auth.exeption.UserAlreadyExists;
import pl.cm.finalprojectbackend.auth.exeption.UserNotFoundException;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.model.UserDTO;
import pl.cm.finalprojectbackend.auth.model.UserDTOBuilder;
import pl.cm.finalprojectbackend.auth.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {

        this.userService = userService;
    }

    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public ResponseEntity getUsers() {

        List<User> users = userService.getUsers();

        if (users.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            user.setPassword("encoded");
            UserDTO userDTO = new UserDTOBuilder().convertUserToUserDTO(user);
            userDTOS.add(userDTO);
        }

        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @RequestMapping(value = {"/user"}, method = RequestMethod.POST)
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public ResponseEntity addUser(@RequestBody UserDTO userDTO, HttpServletResponse response, UriComponentsBuilder uri) {

        try {

            User user = userService.addUser(userDTO.getUsername(), userDTO.getPassword());
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(uri.path("user/{id}").buildAndExpand(user.getId()).toUri());

            return new ResponseEntity(headers, HttpStatus.CREATED);

        } catch (UserAlreadyExists exception) {

            response.setStatus(409);
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Integer id, Principal principal) {

        try {

            userService.deleteUser(id, principal);
            return new ResponseEntity(HttpStatus.OK);

        } catch (UserNotFoundException exception) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } catch (NotEnoughCredentials exception) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public ResponseEntity authorisedUser(Principal principal) {

        if(userService.authorisedUser(principal)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }

}
