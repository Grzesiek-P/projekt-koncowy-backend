package pl.cm.finalprojectbackend.building.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.building.model.Village;

import java.util.Optional;

@Repository
public interface VillageRepository extends JpaRepository<Village, Integer> {

    Optional<Village> findByUser(User user);

}
