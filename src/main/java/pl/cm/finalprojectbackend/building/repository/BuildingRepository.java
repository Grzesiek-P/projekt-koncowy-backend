package pl.cm.finalprojectbackend.building.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.building.model.Building;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.building.model.Village;

import java.util.List;
import java.util.Optional;

@Repository
public interface BuildingRepository extends JpaRepository<Building, Integer> {

    List<Building> findByVillageAndBuildingType(Village village, BuildingType type);

}
