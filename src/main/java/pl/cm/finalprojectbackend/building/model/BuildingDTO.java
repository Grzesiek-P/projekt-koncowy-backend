package pl.cm.finalprojectbackend.building.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuildingDTO {

    @JsonProperty("buildingType")
    private BuildingType buildingType;

}
