package pl.cm.finalprojectbackend.building.model;


public enum BuildingType {
    QUARRY,
    SAWMILL,
    FARM,
    BARRACKS,
    ARMORY,
    STABLES
}
