package pl.cm.finalprojectbackend.building.model;

import java.util.List;

public class VillageDTOBuilder {

    private Integer id;
    private List<Building> buildings;

    public VillageDTOBuilder withId(Integer id){
        this.id = id;
        return this;
    }

    public VillageDTOBuilder withBuildings(List<Building> buildings){
        this.buildings = buildings;
        return this;
    }

    public VillageDTO build(){
        return new VillageDTO(id, buildings);
    }

    public VillageDTO convertVillageToVillageDTO(Village village){
        return withId(village.getId()).withBuildings(village.getBuildings()).build();
    }


}
