package pl.cm.finalprojectbackend.building.model;


import lombok.*;
import pl.cm.finalprojectbackend.auth.model.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "villages")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Village {

    @Id
    @Column(name = "vilage_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "village", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Building> buildings;

    @OneToOne
    private User user;

}


