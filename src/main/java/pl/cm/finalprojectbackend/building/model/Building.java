package pl.cm.finalprojectbackend.building.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "buildings")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Building {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "building_type")
    @Enumerated(value = EnumType.STRING)
    private BuildingType buildingType;

    @ManyToOne
    @JoinColumn(name = "vilage_id")
    @JsonIgnore
    private Village village;

}




