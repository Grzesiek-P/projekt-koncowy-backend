package pl.cm.finalprojectbackend.building.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VillageDTO {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("buildings")
    private List<Building> buildings;

    public VillageDTO(Integer id, List<Building> buildings) {
        this.id = id;
        this.buildings = buildings;
    }


}
