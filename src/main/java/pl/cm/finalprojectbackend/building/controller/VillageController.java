package pl.cm.finalprojectbackend.building.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import pl.cm.finalprojectbackend.building.model.*;
import pl.cm.finalprojectbackend.building.service.VillageService;

import java.security.Principal;
import java.util.Optional;


@RestController
public class VillageController {

    private final VillageService villageService;


    @Autowired
    public VillageController(VillageService villageService) {
        this.villageService = villageService;
    }

    @GetMapping(value = "/village")
    public ResponseEntity getVillage(Principal principal){

        Optional<Village> village = villageService.getVillage(principal);

        if (village.isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(new VillageDTOBuilder().convertVillageToVillageDTO(village.get()), HttpStatus.OK);
    }

    @PostMapping(value = "/village/buildings")
    public ResponseEntity addBuildingToVillage(@RequestBody BuildingDTO buildingDTO, UriComponentsBuilder uri, Principal principal) {

        BuildingType buildingType = buildingDTO.getBuildingType();

        Optional<Building> building = villageService.createBuilding(buildingType, principal);

        if (building.isPresent()) {

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(uri.path("/village/buildings/{id}").buildAndExpand(building.get().getId()).toUri());

            return new ResponseEntity(headers, HttpStatus.CREATED);

        } else {

            return new ResponseEntity((HttpStatus.BAD_REQUEST));

        }


    }

}
