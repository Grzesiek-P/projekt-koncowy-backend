package pl.cm.finalprojectbackend.building.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.Building;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.building.repository.BuildingRepository;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class VillageService {

    private final UserRepository userRepository;
    private final VillageRepository villageRepository;
    private final BuildingRepository buildingRepository;
    private final TreasuryService treasuryService;
    private final GameConfigurationService gameConfigurationService;


    @Autowired
    public VillageService(UserRepository userRepository,
                          VillageRepository villageRepository,
                          BuildingRepository buildingRepository,
                          TreasuryService treasuryService,
                          GameConfigurationService gameConfigurationService) {
        this.userRepository = userRepository;
        this.villageRepository = villageRepository;
        this.buildingRepository = buildingRepository;
        this.treasuryService = treasuryService;
        this.gameConfigurationService = gameConfigurationService;
    }

    public Optional<Village> getVillage(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        return villageRepository.findByUser(user);
    }

    public Optional<Building> createBuilding(BuildingType buildingType, Principal principal) {

        User user = userRepository.findByUsername(principal.getName());
        Optional<Village> opVillage = villageRepository.findByUser(user);
        Village village = opVillage.orElseGet(Village::new);

        boolean isSingletonAndCreated = ((!buildingRepository.findByVillageAndBuildingType(village, buildingType).isEmpty()) &&
                gameConfigurationService.isBuildingSingleton(buildingType));

        List<BuildingType> dependencies = gameConfigurationService.buildingsRequired(buildingType);
        boolean satisfiedDependency = dependencies.isEmpty() ||
                dependencies.stream()
                        .filter(type -> buildingRepository.findByVillageAndBuildingType(village, type).isEmpty())
                        .findAny()
                        .isEmpty();

        if ((!isSingletonAndCreated) && satisfiedDependency && treasuryService.payForBuilding(user, buildingType)) {

            Building building = new Building(0, buildingType, village);

            return Optional.of(buildingRepository.save(building));

        }
        return Optional.empty();
    }

    public boolean isBuilding(BuildingType buildingType, Principal principal){
        User user = userRepository.findByUsername(principal.getName());
        Optional<Village> optionalVillage = villageRepository.findByUser(user);

        return optionalVillage
                .filter(village -> (!buildingRepository.findByVillageAndBuildingType(village, buildingType).isEmpty()))
                .isPresent();
    }

}
