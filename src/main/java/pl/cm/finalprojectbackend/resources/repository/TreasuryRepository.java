package pl.cm.finalprojectbackend.resources.repository;

import org.springframework.data.repository.CrudRepository;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.resources.model.Treasury;

public interface TreasuryRepository extends CrudRepository<Treasury, Integer> {

   Treasury findByUser(User user);
}
