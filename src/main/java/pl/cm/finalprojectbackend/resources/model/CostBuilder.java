package pl.cm.finalprojectbackend.resources.model;

public class CostBuilder {
    private Integer woodCost = 0;
    private Integer stoneCost = 0;
    private Integer foodCost = 0;

    public CostBuilder withWoodCost(Integer woodCost) {
        this.woodCost = woodCost;
        return this;
    }

    public CostBuilder withStoneCost(Integer stoneCost) {
        this.stoneCost = stoneCost;
        return this;
    }

    public CostBuilder withFoodCost(Integer foodCost) {
        this.foodCost = foodCost;
        return this;
    }

    public Cost build() {
        return new Cost(woodCost, stoneCost, foodCost);
    }
}
