package pl.cm.finalprojectbackend.resources.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TreasuryDTO {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("woodAmount")
    private Integer woodAmount;

    @JsonProperty("stoneAmount")
    private Integer stoneAmount;

    @JsonProperty("foodAmount")
    private Integer foodAmount;

    public TreasuryDTO(Integer id, Integer woodAmount, Integer stoneAmount, Integer foodAmount) {
        this.id = id;
        this.woodAmount = woodAmount;
        this.stoneAmount = stoneAmount;
        this.foodAmount = foodAmount;
    }
}
