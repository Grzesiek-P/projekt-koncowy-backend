package pl.cm.finalprojectbackend.resources.model;

public class TreasuryDTOBuilder {
    private Integer id;
    private Integer woodAmount;
    private Integer stoneAmount;
    private Integer foodAmount;

    public TreasuryDTOBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    public TreasuryDTOBuilder withWoodAmount(Integer woodAmount) {
        this.woodAmount = woodAmount;
        return this;
    }

    public TreasuryDTOBuilder withStoneAmount(Integer stoneAmount) {
        this.stoneAmount = stoneAmount;
        return this;
    }

    public TreasuryDTOBuilder withFoodAmount(Integer foodAmount) {
        this.foodAmount = foodAmount;
        return this;
    }

    public TreasuryDTO build() {
        return new TreasuryDTO(id, woodAmount, stoneAmount, foodAmount);
    }

    public TreasuryDTO convertTreasuryToTreasuryDTO(Treasury treasury) {
        return withId(treasury.getId()).withWoodAmount(treasury.getWoodAmount())
                .withStoneAmount(treasury.getStoneAmount()).withFoodAmount(treasury.getFoodAmount()).build();
    }
}
