package pl.cm.finalprojectbackend.resources.model;

import lombok.Getter;

@Getter
public class Cost {
    private final Integer woodCost;
    private final Integer stoneCost;
    private final Integer foodCost;

    public Cost(Integer woodCost, Integer stoneCost, Integer foodCost) {
        this.woodCost = woodCost;
        this.stoneCost = stoneCost;
        this.foodCost = foodCost;
    }
}
