package pl.cm.finalprojectbackend.resources.model;

import lombok.Getter;
import lombok.Setter;
import pl.cm.finalprojectbackend.auth.model.User;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Treasury {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "wood")
    private Integer woodAmount;

    @Column(name = "stone")
    private Integer stoneAmount;

    @Column(name = "food")
    private Integer foodAmount;

    @OneToOne
    private User user;

    /**
     * Initiates a treasury with 50 of each resource.
     */
    public Treasury() {
        woodAmount = 50;
        stoneAmount = 50;
        foodAmount = 50;
    }

    public Treasury(Integer woodAmount, Integer stoneAmount, Integer foodAmount) {
        this.woodAmount = woodAmount;
        this.stoneAmount = stoneAmount;
        this.foodAmount = foodAmount;
    }

}
