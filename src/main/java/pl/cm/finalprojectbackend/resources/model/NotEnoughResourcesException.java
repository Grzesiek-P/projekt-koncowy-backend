package pl.cm.finalprojectbackend.resources.model;

public class NotEnoughResourcesException extends RuntimeException {
    public NotEnoughResourcesException() {
    }
    public NotEnoughResourcesException(String message) {
        super(message);
    }
    public NotEnoughResourcesException(String message, Throwable cause) {
        super(message, cause);
    }
    public NotEnoughResourcesException(Throwable cause) {
        super(cause);
    }
    public NotEnoughResourcesException(String message, Throwable cause,
                                boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
