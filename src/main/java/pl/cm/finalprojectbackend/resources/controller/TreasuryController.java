package pl.cm.finalprojectbackend.resources.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cm.finalprojectbackend.resources.model.Treasury;
import pl.cm.finalprojectbackend.resources.model.TreasuryDTO;
import pl.cm.finalprojectbackend.resources.model.TreasuryDTOBuilder;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;

import java.security.Principal;


@RestController
public class TreasuryController {

    private final TreasuryService treasuryService;

    @Autowired
    public TreasuryController(TreasuryService treasuryService) {
        this.treasuryService = treasuryService;
    }

    @GetMapping(value = "/treasury")
    public TreasuryDTO getTreasury(Principal principal) {

        Treasury treasury = treasuryService.getTreasury(principal);

        return new TreasuryDTOBuilder().convertTreasuryToTreasuryDTO(treasury);
    }
}
