package pl.cm.finalprojectbackend.resources.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.model.UnitType;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.resources.model.*;
import pl.cm.finalprojectbackend.resources.repository.TreasuryRepository;

import java.security.Principal;

@Service
public class TreasuryService {

    private final TreasuryRepository treasuryRepository;
    private final UserRepository userRepository;
    private final CostProvider costProvider;

    @Autowired
    public TreasuryService(TreasuryRepository treasuryRepository, UserRepository userRepository, CostProvider costProvider) {
        this.treasuryRepository = treasuryRepository;
        this.userRepository = userRepository;
        this.costProvider = costProvider;
    }

    public Treasury getTreasury(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());

        return treasuryRepository.findByUser(user);

    }

    public void changeWood(User user, Integer amount) {
        Treasury treasury = treasuryRepository.findByUser(user);
        treasury.setWoodAmount(treasury.getWoodAmount() + amount);
    }

    public void changeStone(User user, Integer amount) {
        Treasury treasury = treasuryRepository.findByUser(user);
        treasury.setStoneAmount(treasury.getStoneAmount() + amount);
    }

    public void changeFood(User user, Integer amount) {
        Treasury treasury = treasuryRepository.findByUser(user);
        treasury.setFoodAmount(treasury.getFoodAmount() + amount);
    }


    public boolean payCost(User user, Cost cost) throws NotEnoughResourcesException {
        Treasury treasury = treasuryRepository.findByUser(user);
        if (cost.getWoodCost() > treasury.getWoodAmount() ||
            cost.getStoneCost() > treasury.getStoneAmount() ||
            cost.getFoodCost() > treasury.getFoodAmount()) {
            return false;
        }
        changeWood(user, -cost.getWoodCost());
        changeStone(user, -cost.getStoneCost());
        changeFood(user, -cost.getFoodCost());
        return true;
    }

    public boolean payForUnit(User user, UnitType unitType) {
        Cost cost = costProvider.getUnitCost(unitType);
        return payCost(user, cost);
    }

    public boolean payForUnit(User user, UnitType unitType, int amount) {
        Cost cost = costProvider.getUnitCost(unitType, amount);
        return payCost(user, cost);
    }

    public boolean payForBuilding(User user, BuildingType buildingType) {
        Cost cost = costProvider.getBuildingCost(buildingType);
        return payCost(user, cost);
    }

}
