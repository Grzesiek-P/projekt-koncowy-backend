package pl.cm.finalprojectbackend.resources.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.model.UnitType;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.model.Cost;
import pl.cm.finalprojectbackend.resources.model.CostBuilder;

@Service
public class CostProvider {

    private  final GameConfigurationService gameConfigurationService;

    @Autowired
    public CostProvider(GameConfigurationService gameConfigurationService) {
        this.gameConfigurationService = gameConfigurationService;
    }

    public Cost getUnitCost(UnitType unitType) {
        return gameConfigurationService.getUnitCost(unitType);
    }

    public Cost getUnitCost(UnitType unitType, int amount) {
        return multipleCost(getUnitCost(unitType), amount);
    }

    public Cost multipleCost(Cost cost, int multiplier) {
        return new CostBuilder().withWoodCost(cost.getWoodCost() * multiplier)
                .withStoneCost(cost.getStoneCost() * multiplier)
                .withFoodCost(cost.getFoodCost() * multiplier).build();
    }

    public Cost getBuildingCost(BuildingType buildingType) {
        return gameConfigurationService.getBuildingCost(buildingType);
    }

}
