package pl.cm.finalprojectbackend.configuration;

import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.model.UnitType;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.resources.model.Cost;
import pl.cm.finalprojectbackend.resources.model.CostBuilder;
import pl.cm.finalprojectbackend.resources.model.Treasury;

import java.util.*;

@Service
public class GameConfigurationService {

    private static final Map<BuildingType, Boolean> singletonBuildings = new HashMap<BuildingType, Boolean>();
    static {
        singletonBuildings.put(BuildingType.QUARRY, false);
        singletonBuildings.put(BuildingType.SAWMILL, false);
        singletonBuildings.put(BuildingType.FARM, false);
        singletonBuildings.put(BuildingType.BARRACKS, true);
        singletonBuildings.put(BuildingType.ARMORY, true);
        singletonBuildings.put(BuildingType.STABLES, true);
    }

    private static final Map<BuildingType, List<BuildingType>> buildingRequirements = new HashMap<BuildingType, List<BuildingType>>();
    static {
        buildingRequirements.put(BuildingType.QUARRY, Arrays.asList());
        buildingRequirements.put(BuildingType.SAWMILL, Arrays.asList());
        buildingRequirements.put(BuildingType.FARM, Arrays.asList());
        buildingRequirements.put(BuildingType.BARRACKS, Arrays.asList());
        buildingRequirements.put(BuildingType.ARMORY, Arrays.asList(BuildingType.BARRACKS));
        buildingRequirements.put(BuildingType.STABLES, Arrays.asList(BuildingType.ARMORY));
    }

    private static final Map<BuildingType, Cost> buildingCost = new HashMap<BuildingType, Cost>();
    static {
        buildingCost.put(BuildingType.QUARRY, new CostBuilder().withWoodCost(80).build());
        buildingCost.put(BuildingType.SAWMILL, new CostBuilder().withWoodCost(20).build());
        buildingCost.put(BuildingType.FARM, new CostBuilder().withWoodCost(20).withStoneCost(20).build());
        buildingCost.put(BuildingType.BARRACKS, new CostBuilder().withWoodCost(40).build());
        buildingCost.put(BuildingType.ARMORY, new CostBuilder().withStoneCost(40).build());
        buildingCost.put(BuildingType.STABLES, new CostBuilder().withWoodCost(30).withStoneCost(40).build());
    }


    private static final Map<BuildingType, Cost> buildingProduction = new HashMap<BuildingType, Cost>();
    static {
        buildingProduction.put(BuildingType.QUARRY, new CostBuilder().withStoneCost(-20).build());
        buildingProduction.put(BuildingType.SAWMILL, new CostBuilder().withWoodCost(-30).build());
        buildingProduction.put(BuildingType.FARM, new CostBuilder().withFoodCost(-30).build());
        buildingProduction.put(BuildingType.BARRACKS, new CostBuilder().build());
        buildingProduction.put(BuildingType.ARMORY, new CostBuilder().build());
        buildingProduction.put(BuildingType.STABLES, new CostBuilder().build());
    }

    private static final Map<UnitType, Cost> unitCost = new HashMap<UnitType, Cost>();
    static {
        unitCost.put(UnitType.INFANTRY, new CostBuilder().withWoodCost(10).build());
        unitCost.put(UnitType.KNIGHT, new CostBuilder().withStoneCost(10).build());
        unitCost.put(UnitType.CAVALRY, new CostBuilder().withWoodCost(10).withStoneCost(10).build());
    }

    private static final Map<UnitType, Cost> unitBoardCost = new HashMap<UnitType, Cost>();
    static {
        unitBoardCost.put(UnitType.INFANTRY, new CostBuilder().withFoodCost(10).build());
        unitBoardCost.put(UnitType.KNIGHT, new CostBuilder().withFoodCost(15).build());
        unitBoardCost.put(UnitType.CAVALRY, new CostBuilder().withFoodCost(20).build());
    }

    private static final Map<UnitType, BuildingType> unitFromBuilding = new HashMap<UnitType, BuildingType>();
    static {
        unitFromBuilding.put(UnitType.KNIGHT, BuildingType.ARMORY);
        unitFromBuilding.put(UnitType.INFANTRY, BuildingType.BARRACKS);
        unitFromBuilding.put(UnitType.CAVALRY, BuildingType.STABLES);
    }



    public boolean isBuildingSingleton (BuildingType buildingType) {
        return singletonBuildings.get(buildingType);
    }


    public List<BuildingType> buildingsRequired (BuildingType buildingType) {
        return buildingRequirements.get(buildingType);
    }

    public Cost getProduction(BuildingType buildingType) {
        return buildingProduction.get(buildingType);
    }

    public Cost getBuildingCost(BuildingType buildingType) {
        return buildingCost.get(buildingType);
    }

    public Cost getUnitCost(UnitType unitType) {
        return unitCost.get(unitType);
    }

    public Cost getUnitBoardCost(UnitType unitType) {
        return unitBoardCost.get(unitType);
    }

    public BuildingType getBuildingTypeForUnit(UnitType unitType) {
        return  unitFromBuilding.get(unitType);
    }

    public Treasury getInitialTreasury() {
        return new Treasury(50, 50,50);
    }

}
