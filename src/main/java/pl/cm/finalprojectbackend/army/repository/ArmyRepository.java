package pl.cm.finalprojectbackend.army.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.auth.model.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArmyRepository extends CrudRepository<Army, Integer> {
    Optional<Army> findByUser(User user);
}
