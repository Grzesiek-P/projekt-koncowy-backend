package pl.cm.finalprojectbackend.army.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.army.model.UnitType;

import java.util.List;

@Repository
public interface UnitRepository extends CrudRepository<Unit, Integer> {
    Unit findByArmy(Army army);
    List<Unit> findByArmyAndUnitType(Army army, UnitType unitType);
}
