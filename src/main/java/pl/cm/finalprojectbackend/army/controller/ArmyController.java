package pl.cm.finalprojectbackend.army.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.cm.finalprojectbackend.army.model.*;
import pl.cm.finalprojectbackend.army.service.ArmyService;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.army.model.Unit;

import java.security.Principal;
import java.util.Optional;

@RestController
public class ArmyController {

    private final ArmyService armyService;


    @Autowired
    public ArmyController(ArmyService armyService) {

        this.armyService = armyService;
    }

    @RequestMapping(value = "/army", method = RequestMethod.GET)
    public ResponseEntity getArmy(Principal principal) {

        try {
            Optional<Army> army = armyService.fetchArmy(principal);
            if (army.isEmpty()) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(new ArmyDTOBuilder().convertArmyToArmyDTO(army.get()), HttpStatus.OK);

        } catch (NotEnoughCredentials ex)
        {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }


    @RequestMapping(value = {"/army/units"}, method = RequestMethod.POST)
    public ResponseEntity addUnitToArmy(@RequestBody UnitDTO unitDTO, UriComponentsBuilder uri, Principal principal) {
        UnitType unitType = unitDTO.getUnitType();

        Optional<Unit> unit = armyService.creatUnit(unitType, principal);

        if (unit.isPresent()) {

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(uri.path("army/units/{id}").buildAndExpand(unit.get().getId()).toUri());

            return new ResponseEntity(headers, HttpStatus.CREATED);

        } else {

            return new ResponseEntity((HttpStatus.BAD_REQUEST));

        }
    }

    @RequestMapping(value = {"/army/{number}"}, method = RequestMethod.POST)
    public ResponseEntity addAllUnitsToArmy(@PathVariable Integer number, @RequestBody UnitDTO unitDTO, UriComponentsBuilder uri, Principal principal) {

        UnitType unitType = unitDTO.getUnitType();

        int numberOfCreated = armyService.creatUnitGivenNumber(unitType, number, principal);

        if (numberOfCreated > 0) {

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(uri.path("/army/{number}").buildAndExpand(numberOfCreated).toUri());

            return new ResponseEntity(headers, HttpStatus.CREATED);
        } else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/army/{number}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUnitFromArmyGivenNumber(@PathVariable Integer number, @RequestBody UnitDTO unitDTO, Principal principal) {

        UnitType unitType = unitDTO.getUnitType();
        int numberOfDeleted = armyService.deleteUnitGivenNumber(unitType, number, principal);

        if (numberOfDeleted > 0) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}
