package pl.cm.finalprojectbackend.army.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.army.model.UnitType;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.army.repository.UnitRepository;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.service.VillageService;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class ArmyService {

    private final UserRepository userRepository;
    private final ArmyRepository armyRepository;
    private final UnitRepository unitRepository;
    private final TreasuryService treasuryService;
    private final VillageService villageService;
    private final GameConfigurationService gameConfigurationService;


    @Autowired
    public ArmyService(UserRepository userRepository,
                       ArmyRepository armyRepository,
                       UnitRepository unitRepository,
                       TreasuryService treasuryService,
                       VillageService villageService,
                       GameConfigurationService gameConfigurationService) {
        this.userRepository = userRepository;
        this.armyRepository = armyRepository;
        this.unitRepository = unitRepository;
        this.villageService = villageService;
        this.treasuryService = treasuryService;
        this.gameConfigurationService = gameConfigurationService;
    }


    public Optional<Army> fetchArmy(Principal principal) {
        if(principal == null) {
            throw new NotEnoughCredentials();
        }

        User user = userRepository.findByUsername(principal.getName());
        return armyRepository.findByUser(user);
    }

    public Optional<Unit> creatUnit(UnitType unitType, Principal principal) {

        User user = userRepository.findByUsername(principal.getName());
        Optional<Army> armyOptional = armyRepository.findByUser(user);
        Army army = armyOptional.orElseGet(Army::new);

        if (villageService.isBuilding(gameConfigurationService.getBuildingTypeForUnit(unitType), principal)
                && treasuryService.payForUnit(user, unitType)) {

            Unit unit = new Unit(0, unitType, army);

            return Optional.of(unitRepository.save(unit));
        }

        return Optional.empty();
    }

    public int creatUnitGivenNumber(UnitType unitType, int number, Principal principal) {

        User user = userRepository.findByUsername(principal.getName());
        Optional<Army> armyOptional = armyRepository.findByUser(user);
        Army army = armyOptional.orElseGet(Army::new);

        if (villageService.isBuilding(gameConfigurationService.getBuildingTypeForUnit(unitType), principal)
                && treasuryService.payForUnit(user, unitType, number)) {

            int numberOfCreated = 0;

            for (int k = 0; k < number; k++) {
                Unit unit = new Unit(0, unitType, army);
                unitRepository.save(unit);
                numberOfCreated++;
            }
            return numberOfCreated;
        }
        return 0;
    }


    public int deleteUnitGivenNumber(UnitType unitType, int number, Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        Optional<Army> armyOptional = armyRepository.findByUser(user);

        if(armyOptional.isPresent()) {
            Army army = armyOptional.get();
            List<Unit> units = unitRepository.findByArmyAndUnitType(army, unitType);
            if (units.size() < number) {
                unitRepository.deleteAll(units);
            } else {
                for (int k = 0; k < number; k++) {
                    unitRepository.delete(units.get(k));
                }
            }
            return Math.min(units.size(),number);
        }

        return 0;
    }
}
