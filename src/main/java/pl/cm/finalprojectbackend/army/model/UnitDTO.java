package pl.cm.finalprojectbackend.army.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

public class UnitDTO {
    @Getter
    @Setter
    @JsonProperty("unitType")
    private UnitType unitType;

}
