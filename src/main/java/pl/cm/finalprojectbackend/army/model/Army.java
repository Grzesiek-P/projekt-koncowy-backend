package pl.cm.finalprojectbackend.army.model;

import lombok.Getter;
import lombok.Setter;
import pl.cm.finalprojectbackend.auth.model.User;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Army {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToMany(mappedBy = "army", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Unit> units;

    @OneToOne
    private User user;
}
