package pl.cm.finalprojectbackend.army.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "unitType")
    @Enumerated(EnumType.STRING)
    private UnitType unitType;

    @ManyToOne
    @JoinColumn(name = "army")
    @JsonIgnore
    private Army army;
}
