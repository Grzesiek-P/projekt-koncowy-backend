package pl.cm.finalprojectbackend.army.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ArmyDTO {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("units")
    private List<Unit> units;

    public ArmyDTO(Integer id, List<Unit> units) {
        this.id = id;
        this.units = units;
    }
}
