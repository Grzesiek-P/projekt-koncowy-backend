package pl.cm.finalprojectbackend.army.model;

public enum UnitType {
    INFANTRY,
    KNIGHT,
    CAVALRY
}
