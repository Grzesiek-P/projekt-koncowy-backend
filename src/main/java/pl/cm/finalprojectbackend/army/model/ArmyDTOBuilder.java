package pl.cm.finalprojectbackend.army.model;

import java.util.List;

public class ArmyDTOBuilder {
    private Integer id;
    private List<Unit> units;

    public ArmyDTOBuilder withId(Integer id){
        this.id = id;
        return this;
    }

    public ArmyDTOBuilder withUnits(List<Unit> units){
        this.units = units;
        return this;
    }

    public ArmyDTO build(){
        return new ArmyDTO(id, units);
    }

    public ArmyDTO convertArmyToArmyDTO(Army army){
        return withId(army.getId()).withUnits(army.getUnits()).build();
    }
}
