package pl.cm.finalprojectbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class FinalprojectbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalprojectbackendApplication.class, args);
	}

}
