package pl.cm.finalprojectbackend.building.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.Building;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.building.model.Village;
import pl.cm.finalprojectbackend.building.repository.BuildingRepository;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public class VillageServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private VillageRepository villageRepository;
    @Mock
    private BuildingRepository buildingRepository;
    @Mock
    private TreasuryService treasuryService;
    @Mock
    private GameConfigurationService gameConfigurationService;

    @InjectMocks
    private VillageService villageService;

    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getVillage_whenGivenUser_ThenGetsVillage() {
        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        Village village = Mockito.mock(Village.class);
        Optional<Village> optional = Optional.of(village);
        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);

        Optional<Village> result = villageService.getVillage(principal);

        assertThat(result).isEqualTo(optional);
        verify(userRepository, times(1)).findByUsername(username);
        verify(principal, times(1)).getName();

    }

    @Test
    public void createBuilding_WhenNotEnoughTreasury_ThenOptionalEmpty() {

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        BuildingType buildingType = BuildingType.QUARRY;

        Village village = Mockito.mock(Village.class);

        Optional<Village> optional = Optional.of(village);
        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingType)).thenReturn(new ArrayList<>());
        Mockito.when(gameConfigurationService.isBuildingSingleton(buildingType)).thenReturn(false);
        Mockito.when(gameConfigurationService.buildingsRequired(buildingType)).thenReturn(new ArrayList<>());

        Mockito.when(treasuryService.payForBuilding(user, buildingType)).thenReturn(false);

        Optional<Building> result = villageService.createBuilding(buildingType, principal);

        assertThat(result).isEqualTo(Optional.empty());
        verify(userRepository, times(1)).findByUsername(username);
        verify(principal, times(1)).getName();

    }

    @Test
    public void createBuilding_WhenEnoughTreasury_ThenOptionalNonEmpty() {

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        BuildingType buildingType = BuildingType.QUARRY;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingType)).thenReturn(new ArrayList<>());
        Mockito.when(gameConfigurationService.isBuildingSingleton(buildingType)).thenReturn(false);
        Mockito.when(gameConfigurationService.buildingsRequired(buildingType)).thenReturn(new ArrayList<>());

        Mockito.when(treasuryService.payForBuilding(user, buildingType)).thenReturn(true);
        Mockito.when(buildingRepository.save(any(Building.class))).thenReturn(building);

        Optional<Building> result = villageService.createBuilding(buildingType, principal);

        assertThat(result.isEmpty()).isFalse();
        verify(buildingRepository, times(1)).save(any(Building.class));

    }



    @Test
    public void createBuilding_WhenEnoughTreasuryDependencySatisfiedAndSingletonAndNoBuilt_ThenOptionalNonEmpty() {

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        BuildingType buildingTypePrevious = BuildingType.BARRACKS;
        BuildingType buildingTypeToBuild = BuildingType.ARMORY;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypeToBuild)).thenReturn(new ArrayList<>());
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypePrevious)).thenReturn(Arrays.asList(building));
        Mockito.when(gameConfigurationService.isBuildingSingleton(buildingTypeToBuild)).thenReturn(true);
        Mockito.when(gameConfigurationService.buildingsRequired(buildingTypeToBuild)).thenReturn(Arrays.asList(buildingTypePrevious));

        Mockito.when(treasuryService.payForBuilding(user, buildingTypeToBuild)).thenReturn(true);
        Mockito.when(buildingRepository.save(any(Building.class))).thenReturn(building);

        Optional<Building> result = villageService.createBuilding(buildingTypeToBuild, principal);

        assertThat(result.isEmpty()).isFalse();
        verify(buildingRepository, times(1)).save(any(Building.class));

    }

    @Test
    public void createBuilding_WhenEnoughTreasuryDependencySatisfiedAndSingletonAndBuilt_ThenOptionalEmpty() {

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        BuildingType buildingTypePrevious = BuildingType.BARRACKS;
        BuildingType buildingTypeToBuild = BuildingType.ARMORY;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypeToBuild)).thenReturn(Arrays.asList(building));
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypePrevious)).thenReturn(Arrays.asList(building));
        Mockito.when(gameConfigurationService.isBuildingSingleton(buildingTypeToBuild)).thenReturn(true);
        Mockito.when(gameConfigurationService.buildingsRequired(buildingTypeToBuild)).thenReturn(Arrays.asList(buildingTypePrevious));

        Mockito.when(treasuryService.payForBuilding(user, buildingTypeToBuild)).thenReturn(true);
        Mockito.when(buildingRepository.save(any(Building.class))).thenReturn(building);

        Optional<Building> result = villageService.createBuilding(buildingTypeToBuild, principal);

        assertThat(result).isEqualTo(Optional.empty());
        verify(buildingRepository, times(0)).save(any(Building.class));

    }

    @Test
    public void createBuilding_WhenEnoughTreasuryDependencyNotSatisfiedAndSingletonAndNotBuilt_ThenOptionalEmpty() {

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        BuildingType buildingTypePrevious = BuildingType.BARRACKS;
        BuildingType buildingTypeToBuild = BuildingType.ARMORY;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypeToBuild)).thenReturn(Arrays.asList(building));
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingTypePrevious)).thenReturn(new ArrayList<>());
        Mockito.when(gameConfigurationService.isBuildingSingleton(buildingTypeToBuild)).thenReturn(true);
        Mockito.when(gameConfigurationService.buildingsRequired(buildingTypeToBuild)).thenReturn(Arrays.asList(buildingTypePrevious));

        Mockito.when(treasuryService.payForBuilding(user, buildingTypeToBuild)).thenReturn(true);
        Mockito.when(buildingRepository.save(any(Building.class))).thenReturn(building);

        Optional<Building> result = villageService.createBuilding(buildingTypeToBuild, principal);

        assertThat(result).isEqualTo(Optional.empty());
        verify(buildingRepository, times(0)).save(any(Building.class));

    }

    @Test
    public void isBuilding_buildingNotPresent_ThenFalse(){

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";

        BuildingType buildingType = BuildingType.BARRACKS;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingType)).thenReturn(new ArrayList<>());

        boolean result = villageService.isBuilding(buildingType, principal);

        assertThat(result).isFalse();
    }

    @Test
    public void isBuilding_buildingPresent_ThenTrue(){

        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";

        BuildingType buildingType = BuildingType.BARRACKS;

        Village village = Mockito.mock(Village.class);
        Building building = Mockito.mock(Building.class);
        Optional<Village> optional = Optional.of(village);

        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(villageRepository.findByUser(user)).thenReturn(optional);
        Mockito.when(buildingRepository.findByVillageAndBuildingType(village, buildingType)).thenReturn(Arrays.asList(building));

        boolean result = villageService.isBuilding(buildingType, principal);

        assertThat(result).isTrue();
    }


}


/* Tests
Method getVillage()
1) given user gets village

Method creatBuilding
1) given user, satisfied dependencies and no singleton and enough treasury then non empty
2) given user, satisfied dependencies, no singleton and not enough treasury then empty
3) given user, satisfied dependencies and singleton and not build and enough treasury then non empty
4) given user, satisfied dependencies, singleton and build  and enough treasury then empty
5) given user, not satisfied dependencies, singleton and build  and enough treasury then empty

Method isBuilding
1) given user, given building type present then true
1) given user, given building type not present then false

 */
