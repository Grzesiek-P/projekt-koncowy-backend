package pl.cm.finalprojectbackend.army.service;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import pl.cm.finalprojectbackend.army.model.Army;
import pl.cm.finalprojectbackend.army.model.Unit;
import pl.cm.finalprojectbackend.army.model.UnitType;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.army.repository.UnitRepository;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.model.BuildingType;
import pl.cm.finalprojectbackend.building.service.VillageService;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.service.TreasuryService;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.security.Principal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;


public class ArmyServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private ArmyRepository armyRepository;
    @Mock
    private UnitRepository unitRepository;
    @Mock
    private TreasuryService treasuryService;
    @Mock
    private VillageService villageService;
    @Mock
    private GameConfigurationService gameConfigurationService;

    @InjectMocks
    private ArmyService armyService;

    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fetchArmy_getsArmy() {
        Principal principal = Mockito.mock(Principal.class);
        User user = Mockito.mock(User.class);
        String username = "username";
        Army army = Mockito.mock(Army.class);
        Optional<Army> optional = Optional.of(army);
        Mockito.when(principal.getName()).thenReturn(username);
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(armyRepository.findByUser(user)).thenReturn(optional);

        Optional<Army> result = armyService.fetchArmy(principal);

        assertThat(result).isEqualTo(optional);
        verify(userRepository, times(1)).findByUsername(username);
        verify(principal, times(1)).getName();

    }

    @Test
    public void fetchArmy_throwsException_givenNull() {
        Principal principal = null;

        assertThatThrownBy(() -> armyService.fetchArmy(principal)).hasSameClassAs(new NotEnoughCredentials());
    }

    @Test
    public void createUnit_returnsPresentOptional_givenGoodRequest() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        Unit unit = mock(Unit.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(true);
        when(treasuryService.payForUnit(user, type)).thenReturn(true);
        when(unitRepository.save(any(Unit.class))).thenReturn(unit);
        when(unit.getUnitType()).thenReturn(type);

        Optional<Unit> result = armyService.creatUnit(type, principal);

        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getUnitType()).isEqualTo(type);
        verify(unitRepository, times(1)).save(any(Unit.class));
    }

    @Test
    public void createUnit_returnsEmptyOptional_whenCannotAfford() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        Unit unit = mock(Unit.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(true);
        when(treasuryService.payForUnit(user, type)).thenReturn(false);
        when(unitRepository.save(any(Unit.class))).thenReturn(unit);
        when(unit.getUnitType()).thenReturn(type);

        Optional<Unit> result = armyService.creatUnit(type, principal);

        assertThat(result.isEmpty()).isTrue();
        verify(unitRepository, times(0)).save(any(Unit.class));
    }

    @Test
    public void createUnit_returnsEmptyOptional_whenNoRecruitmentBuilding() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        Unit unit = mock(Unit.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(false);
        when(treasuryService.payForUnit(user, type)).thenReturn(true);
        when(unitRepository.save(any(Unit.class))).thenReturn(unit);
        when(unit.getUnitType()).thenReturn(type);

        Optional<Unit> result = armyService.creatUnit(type, principal);

        assertThat(result.isEmpty()).isTrue();
        verify(unitRepository, times(0)).save(any(Unit.class));
    }

    @Test
    public void createUnitGivenNumber_returnsCreatedCount_whenBuildingInfantry() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 2;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(true);
        when(treasuryService.payForUnit(user, type, number)).thenReturn(true);

        int result = armyService.creatUnitGivenNumber(type, number, principal);

        ArgumentCaptor<Unit> argument = ArgumentCaptor.forClass(Unit.class);
        assertThat(result).isEqualTo(number);
        verify(unitRepository, times(number)).save(argument.capture());
        assertThat(argument.getAllValues().size()).isEqualTo(2);
        argument.getAllValues().forEach(unit -> {
            assertThat(unit.getUnitType()).isEqualTo(type);
        });
    }

    @Test
    public void createUnitGivenNumber_returns0_whenCannotPay() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 2;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(true);
        when(treasuryService.payForUnit(user, type, number)).thenReturn(false);

        int result = armyService.creatUnitGivenNumber(type, number, principal);

        assertThat(result).isEqualTo(0);
        verify(unitRepository, times(0)).save(any(Unit.class));
    }

    @Test
    public void createUnitGivenNumber_returns0_whenNoRecruitmentBuilding() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 2;
        String username = "username";
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(gameConfigurationService.getBuildingTypeForUnit(type)).thenReturn(BuildingType.BARRACKS);
        when(villageService.isBuilding(BuildingType.BARRACKS, principal)).thenReturn(false);
        when(treasuryService.payForUnit(user, type, number)).thenReturn(true);

        int result = armyService.creatUnitGivenNumber(type, number, principal);

        assertThat(result).isEqualTo(0);
        verify(unitRepository, times(0)).save(any(Unit.class));
    }

    @Test
    public void deleteUnitGivenNumber_deletesUnits_whenMoreUnitsThanToDelete() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 2;
        String username = "username";
        Army army = mock(Army.class);
        Unit unit1 = mock(Unit.class);
        Unit unit2 = mock(Unit.class);
        Unit unit3 = mock(Unit.class);
        List<Unit> units = new LinkedList();
        units.add(unit1);
        units.add(unit2);
        units.add(unit3);
        Optional<Army> optionalArmy = Optional.of(army);
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(armyRepository.findByUser(user)).thenReturn(optionalArmy);
        when(unitRepository.findByArmyAndUnitType(army, type)).thenReturn(units);

        int result = armyService.deleteUnitGivenNumber(type, number, principal);

        assertThat(result).isEqualTo(number);
        ArgumentCaptor<Unit> argument = ArgumentCaptor.forClass(Unit.class);
        verify(unitRepository, times(number)).delete(argument.capture());
        assertThat(argument.getAllValues()).containsExactly(unit1, unit2);
    }

    @Test
    public void deleteUnitGivenNumber_deletesUnits_whenLessUnitsThanToDelete() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 4;
        String username = "username";
        Army army = mock(Army.class);
        Unit unit1 = mock(Unit.class);
        Unit unit2 = mock(Unit.class);
        Unit unit3 = mock(Unit.class);
        List<Unit> units = new LinkedList();
        units.add(unit1);
        units.add(unit2);
        units.add(unit3);
        Optional<Army> optionalArmy = Optional.of(army);
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(armyRepository.findByUser(user)).thenReturn(optionalArmy);
        when(unitRepository.findByArmyAndUnitType(army, type)).thenReturn(units);

        int result = armyService.deleteUnitGivenNumber(type, number, principal);

        assertThat(result).isEqualTo(units.size());
        verify(unitRepository, times(1)).deleteAll(units);
    }

    @Test
    public void deleteUnitGivenNumber_returns0_givenWrongReqest() {
        User user = mock(User.class);
        UnitType type = UnitType.INFANTRY;
        int number = 4;
        String username = "username";
        Optional<Army> optionalArmy = Optional.empty();
        Principal principal = Mockito.mock(Principal.class);
        when(principal.getName()).thenReturn(username);
        when(userRepository.findByUsername(username)).thenReturn(user);
        when(armyRepository.findByUser(user)).thenReturn(optionalArmy);

        int result = armyService.deleteUnitGivenNumber(type, number, principal);

        assertThat(result).isEqualTo(0);
    }


}
