package pl.cm.finalprojectbackend.auth.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import pl.cm.finalprojectbackend.army.repository.ArmyRepository;
import pl.cm.finalprojectbackend.auth.exeption.NotEnoughCredentials;
import pl.cm.finalprojectbackend.auth.exeption.UserAlreadyExists;
import pl.cm.finalprojectbackend.auth.exeption.UserNotFoundException;
import pl.cm.finalprojectbackend.auth.model.User;
import pl.cm.finalprojectbackend.auth.model.UserRole;
import pl.cm.finalprojectbackend.auth.repository.UserRepository;
import pl.cm.finalprojectbackend.building.repository.VillageRepository;
import pl.cm.finalprojectbackend.configuration.GameConfigurationService;
import pl.cm.finalprojectbackend.resources.model.Treasury;
import pl.cm.finalprojectbackend.resources.repository.TreasuryRepository;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

public class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private TreasuryRepository treasuryRepository;
    @Mock
    private GameConfigurationService gameConfigurationService;

    @InjectMocks
    private UserService userService;

    @Before
    public void before() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUsers_returnsListOfUsers(){
        User user = Mockito.mock(User.class);
        userRepository.save(user);
        Mockito.when(userRepository.findAll()).thenReturn(List.of(user));

        List<User> users = userService.getUsers();

        Assertions.assertThat(users).isEqualTo(List.of(user));
    }

    @Test
    public void addUser_returnsNewUser(){
        String username = "username";
        String password = "password";
        User user = Mockito.mock(User.class);
        userRepository.save(user);
        Treasury treasury = Mockito.mock(Treasury.class);
        Mockito.when(gameConfigurationService.getInitialTreasury()).thenReturn(treasury);
        Mockito.when(treasuryRepository.save(any(Treasury.class))).thenReturn(treasury);
        Mockito.when(userRepository.findAll()).thenReturn(List.of(user));

        User newUser = userService.addUser(username, password);

        Assertions.assertThat(newUser.getUsername()).isEqualTo(username);
        Mockito.verify(userRepository, Mockito.times(2)).save(any(User.class));
        Mockito.verify(passwordEncoder, Mockito.times(1)).encode("password");
    }

    @Test
    public void addUser_throwsUserAlreadyExistsException_givenTheSameUsername(){
        String username = "username";
        String password = "password";
        User user = Mockito.mock(User.class);
        Treasury treasury = Mockito.mock(Treasury.class);
        Mockito.when(user.getUsername()).thenReturn("username");
        Mockito.when(user.getPassword()).thenReturn("password");
        Mockito.when(gameConfigurationService.getInitialTreasury()).thenReturn(treasury);
        Mockito.when(treasuryRepository.save(any(Treasury.class))).thenReturn(treasury);
        Mockito.when(userRepository.findByUsername("username")).thenReturn(user);
        userRepository.save(user);

        Assertions.assertThatThrownBy(() -> userService.addUser(username,password)).hasSameClassAs(new UserAlreadyExists());
    }

    @Test
    public void authorisedUser_returnsTrue_givenAuthorizedUser(){
        Principal principal = Mockito.mock(Principal.class);
        String username = "username";
        Mockito.when(principal.getName()).thenReturn(username);

        Assertions.assertThat(userService.authorisedUser(principal)).isTrue();
    }

    @Test
    public void authorisedUser_returnsFalse_givenUnauthorizedUser(){
        Principal principal = null;

        Assertions.assertThat(userService.authorisedUser(principal)).isFalse();
    }

    @Test
    public void deleteUser_returnsDeletedUser(){
        User user = Mockito.mock(User.class);
        userRepository.save(user);
        String username = "username";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(user.getUserRole()).thenReturn(UserRole.USER);
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));
        List<User> users = userService.getUsers();
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getName()).thenReturn(username);

        User deletedUser = userService.deleteUser(1,principal);

        Assertions.assertThat(deletedUser).isEqualTo(user);
        Assertions.assertThat(users).isEmpty();
        Mockito.verify(userRepository, Mockito.times(1)).delete(user);
    }

    @Test
    public void deleteUser_throwsUserNotFoundException_givenNewUser(){
        User user = Mockito.mock(User.class);
        User user2 = Mockito.mock(User.class);
        userRepository.save(user);
        String username = "username";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(user.getUserRole()).thenReturn(UserRole.USER);
        Mockito.when(user2.getUserRole()).thenReturn(UserRole.USER);
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getName()).thenReturn("username");

        Assertions.assertThatThrownBy(() -> userService.deleteUser(2,principal)).hasSameClassAs(new UserNotFoundException());
    }

    @Test
    public void deleteUser_throwsNotEnoughCredentialsException_givenBadUser(){
        User user = Mockito.mock(User.class);
        User user2 = Mockito.mock(User.class);
        userRepository.save(user);
        String username = "username";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(user.getUserRole()).thenReturn(UserRole.USER);
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user2));
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getName()).thenReturn("username");

        Assertions.assertThatThrownBy(() -> userService.deleteUser(1,principal)).hasSameClassAs(new NotEnoughCredentials());
    }

    @Test
    public void deleteUser_returnsDeletedUser_givenAdminUser(){
        User user = Mockito.mock(User.class);
        User admin = Mockito.mock(User.class);
        userRepository.save(user);
        userRepository.save(admin);
        String username = "username";
        String usernameAdmin = "admin";
        Mockito.when(userRepository.findByUsername(username)).thenReturn(user);
        Mockito.when(userRepository.findByUsername(usernameAdmin)).thenReturn(admin);
        Mockito.when(user.getUserRole()).thenReturn(UserRole.USER);
        Mockito.when(admin.getUserRole()).thenReturn(UserRole.ADMIN);
        Mockito.when(userRepository.findById(1)).thenReturn(Optional.of(user));
        Mockito.when(userRepository.findById(2)).thenReturn(Optional.of(admin));
        Mockito.when(userService.getUsers()).thenReturn(List.of(user,admin));
        Principal principal = Mockito.mock(Principal.class);
        Mockito.when(principal.getName()).thenReturn(usernameAdmin);

        User deletedUser = userService.deleteUser(2,principal);

        Assertions.assertThat(deletedUser).isEqualTo(admin);
        Mockito.verify(userRepository, Mockito.times(1)).delete(admin);
    }
}
