FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y default-jdk
COPY finalprojectbackend-0.0.1-SNAPSHOT.jar /
CMD ["java", "-jar", "finalprojectbackend-0.0.1-SNAPSHOT.jar"]
